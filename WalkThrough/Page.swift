//
//  Page.swift
//  WalkThrough
//
//  Created by Ezaden Seraj on 28/10/2017.
//  Copyright © 2017 Ezaden Seraj. All rights reserved.
//

import Foundation

struct Page {
    let imageName: String
    let headerText: String
    let bodyText: String
    
    init(imageName: String, headerText: String, bodyText: String) {
        self.imageName = imageName
        self.headerText = headerText
        self.bodyText = bodyText
    }
}
